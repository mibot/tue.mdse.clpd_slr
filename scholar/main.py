# coding=utf-8
import requests
import os
from mstranslator import Translator
from unicodeManager import UnicodeReader, UnicodeWriter
from scholar.download import DownloadPDF

dataPath = os.path.abspath(os.path.relpath('../data'))
translator = Translator('df27cb400f5b42ff8f7a5535235c7269')

f = open(os.path.join(dataPath, 'results', 'mostCommonDomains.csv'), 'rb')
domains = UnicodeReader(f) 
    
def main():
    f = open(os.path.join(dataPath, 'results', 'resolved_query_unique_id.csv'), 'rb')
    reader = UnicodeReader(f)    
    
    for row in reader:
        p = False
        downloaded = "False"
        count = 0
        id = row[0] 
        print id
        try:   
            title = row[2]
            print title
            url = row[8]        
            while downloaded == "False" and count < 2:
                count += 1
                source = _getSource(url)            
                url = row[6]        
                #source = _getSource(url) 
                res_title = _checkTitle(title)
                #res_source = _checkSource(source)
                if res_title:
                    print url 
                    s = DownloadPDF()
                    p = s.download(url, destination= '../data/pdf/tocheck', path = str(id) +'.pdf' )
                    if p == True:
                        downloaded = "True"
                    else:
                        url = row[6]
                         
        except UnicodeDecodeError:
            pass                 
            
        with open(os.path.join(dataPath, 'results', 'resolved_query_processed.csv'), 'ab') as g:
                writer = UnicodeWriter(g)
                writer.writerow(str(id).split() + downloaded.split())
    
        
            

def _checkTitle(title):  
    res = ""
    
    try:
        res = translator.detect_langs([title])    
    except (IndexError, ValueError):        
        pass
    return res

def _getSource(url):
    res = ""
    try: 
        res = url.split("/")[2]
    except (IndexError, ValueError):        
        pass
    return res

def _checkSource(source):
    res = ""
#     f = open(os.path.join(dataPath, 'results', 'mostCommonDomains.csv'), 'rb')
#     reader = UnicodeReader(f)  
    
    for domain in domains:
        if source in domain:
            res = 'download'
            return res     
    return res

if __name__ == '__main__':
    main()        
        