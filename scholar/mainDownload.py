#!/usr/bin/env python
# -*- coding: utf-8 -*-
import MySQLdb
import requests
import os
from mstranslator import Translator
from unicodeManager import UnicodeReader, UnicodeWriter
from scholar.download import DownloadPDF
from scholar.languageDetetion import LanguageDetection
from scholar.filter import Filter
from random import randint
import time
from langdetect import language
import re
import random

dataPath = os.path.abspath(os.path.relpath('../data'))
key_choices = ["dfba48d7a4414260a3210ec13a1d3383",
                  "130e090dd86c4fd0837995ad6524acd9",
                  "9df3e0d256074f28b1c2a98048b02628",                                   
                 "ef187a977c38470487c2622e56699a35",
                 "9dd0911b0d3d4cd5897d5807414c1d37",
                 "e45450779637461daa4c8cbe881ef750",
                 "a77b420016874ba087039c230a91ed47",
                 "d676bbb1402b4e2889ed48e7cac0ffe6",
                 "5715717ae2dd484192d2dbe8f642f6f5",
                 "24139b3ec0b7407db2ea1659a14be0f0", ]





f = open(os.path.join(dataPath, 'results', 'mostCommonDomains.csv'), 'rb')
domains = UnicodeReader(f) 


db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user="root",         # your username
                     passwd="master",  # your password
                     db="clpd")        # name of the data base
                     

cur = db.cursor()
    
def downloadPDF():
    i = 29727
    nrows = _getNumPub()
    #nrows = 25000 #15000
     
     
    while i <= nrows:
        p = False
        downloaded = "False"
        count = 0
        if _checkPDFDownloaded(i):
            if not _checkPDFinURL(i):# or not _checkPDFinURL(i):            
                try:   
                    title = _getTitle(i)
                    #print title
                    res_title = _checkTitle(title)
                    if res_title: #or not res_title: 
                                        
                        url = _getUrl(i, "direct_link")
                                  
                        while downloaded == "False" and count < 2:
                            count += 1
                            if url:
                                s = DownloadPDF()
                                p = s.download(url, destination= '../data/pdf/tocheck', path = str(i) +'.pdf' )
                            if p == True:
                                downloaded = "True"
                            else:
                                url = _getUrl(i, "main_link")
                             
                                  
                except UnicodeDecodeError:
                    pass                 
                     
                if downloaded == "True":
                    sql = "update resolved_query set downloaded = 1 where id = %s" %(i) 
                     
                    try:
                        cur.execute(sql)
                        db.commit()
                    except:
                        db.rollback()
        print "Id: %s. Downloaded: %s" %(i, downloaded)    
        i += 1
        
def _checkPDFDownloaded(id):    
    sql = 'select id from resolved_query where downloaded = 0 and id = %s;' %(id)
    cur.execute(sql)    
    try: 
        return cur.fetchall()[0][0]
    except IndexError:
        return ""     
    
def _checkPDFinURL(id):    
    sql = 'select id from resolved_query where (direct_link like "%%pdf%%" or main_link like "%%pdf%%") and downloaded = 0 and id = %s;' %(id)
    cur.execute(sql)    
    try: 
        return cur.fetchall()[0][0]
    except IndexError:
        return ""     
    
def _getTitle(id):
    sql = 'select title from resolved_query where id = %s;' %(id)
    cur.execute(sql)    
    return cur.fetchall()[0][0] 

def _checkTitle(title):  
    res = ""
    
    try:
        translator = Translator( random.choice(key_choices))
        res = translator.detect_langs([title])    
    except (IndexError, ValueError):        
        pass
    return res

def _getUrl(id, linkName):
    sql = 'select %s from resolved_query where id = %s;' %(linkName, id)
    cur.execute(sql)    
    return cur.fetchall()[0][0]

def _getSource(url):
    res = ""
    try: 
        res = url.split("/")[2]
    except (IndexError, ValueError):        
        pass
    return res

def _checkSource(source):
    res = ""
    f = open(os.path.join(dataPath, 'results', 'mostCommonDomains.csv'), 'rb')
    reader = UnicodeReader(f)  
    
    for domain in domains:
        if source in domain:
            res = 'download'
            return res     
    return res

def filterPDF():
    
    i = 1#12590
    nrows = _getNumPub()
    #nrows = 110 
    while i <= nrows:
        res = None
        
        if  _checkDownloaded(i):
            f = Filter()
            #f._updateNumPages(i)
            #f._classify(i)
            #f._prepareTail(i)
            #f.countOccurencies(i)
            #f.countOccurencies(i, "paper")
            #f.countOccurencies(i, "thesis")
            #print (f._parse_toc(i))
           # f._filterPub(i, 1)
            f._onlyPapers(i) 
#             
#             l = LanguageDetection()
#             res = l._detection(i)
#             
#             f = Filter()
#             x = f._classify(i)
# #             if res == "English":
# #                 sql = "update resolved_query set english = 1 where id = %s" %(i) 
# #                 
# #                 try:
# #                     cur.execute(sql)
# #                     db.commit()
# #                 except:
# #                     db.rollback()
                
            print "File: %s." %(i)             
        i += 1
        
def _checkDownloaded(id):
    sql = 'select id from resolved_query where downloaded = 1 and pdf2text = 1 and english = 1 and id = %s;' %(id)
    cur.execute(sql)    
    try: 
        return cur.fetchall()[0][0]
    except IndexError:
        return ""    
        
def _getNumPub():
    sql = 'select count(*) from resolved_query;'              
    cur.execute(sql)
    return cur.fetchall()[0][0]



def languageDetection():
    lang = ""
    
    i = 1
    nrows = _getNumPub()
    while i <= nrows:
        lang = None
        id = ""
        english = 0
        other = 0
        text = ""   
        res = ""     
        sql = "select id from resolved_query where downloaded = 1 and pdf2text = 1 and english = 0 and id = %s"  %(i)
        cur.execute(sql)
        try: 
            id = cur.fetchall()[0][0]
        except IndexError:
            pass
        if id:
            
            with open(os.path.join(dataPath, 'txt', str(id) +'.txt')) as infile:
                for line in infile:
                    if not re.match(r'^\s*$', line):                                              
                        line = re.sub(r"-\n","", line)
                        line = re.sub(r"\n"," ", line) 
                        text += line
                infile.close()
            lenText = len(text)
            
            nrequest = round(float(lenText)/5000)
            #print text
            #print lenText
            #print nrequest
            count = 1
            while count <= nrequest:
                content = ""
                
                posIni = (count * 5000) - 5000
                posFin = (count * 5000) - 1
                while posIni <= posFin:
                    try:
                        content += str(text[posIni]).encode("utf8")
                    except:
                        pass
                     
                    posIni += 1
                #print content
                try:
                    translator = Translator( random.choice(key_choices))
                    res = translator.detect_lang([content])
                            
                except:                        
                    pass
                #print res
                if res:
                    if res == 'en': english += 1
                    else: other += 1
                count += 1
            if english > other:
                lang = "English"
                sql = "update resolved_query set english = 1 where id = %s" %(i)
            else: lang = "Other"                
            try:
                cur.execute(sql)
                db.commit()
            except:
                db.rollback()
        print "Id: %s. Language: %s" %(i, lang)    
        i += 1
    print "Done!"

def revisores():
    db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user="root",         # your username
                     passwd="master",  # your password
                     db="citt2018")        # name of the data base
    cur = db.cursor()
                     
    sql = 'select nombre, apellidos, afiliacion1, pais1, afiliacion2, pais2 from revisor order by apellidos;'
    #print sql           
    try:            
        cur.execute(sql)
        res = cur.fetchall()
    except:
        res = ""
    for row in res:    
        #!/usr/bin/env python
        # -*- coding: utf-8 -*-
        
        if row[4]:
            
            print row[0].decode("latin-1").encode("utf8") + " "  + row[1].decode("latin-1").encode("utf8") + ", " + row[2].decode("latin-1").encode("utf8") +", " + row[3] + " / " + row[4].decode("latin-1").encode("utf8") +", " + row[5] + "."
        else:
            print row[0].decode("latin-1").encode("utf8") + " "  + row[1].decode("latin-1").encode("utf8") + ", " + row[2].decode("latin-1").encode("utf8") +", " + row[3] + "."
        
     
def checkEmptyFiles():   
     i = 1
     while i <= 30051:
        #if os.path.join(dataPath, 'txt', str(id) +'.txt').st_size == 0:
        try:
            file = os.path.join(dataPath, 'txt', str(i) +'.txt')   
            print i, os.stat(file).st_size
            
            if os.stat(file).st_size == 0:
                print i
        except:
            pass             
        i+=1

if __name__ == '__main__':
    #downloadPDF()
    #languageDetection()
    #filterPDF()
    #revisores()
    checkEmptyFiles()
#     time.sleep(20)
#     print "Language Detection process starts now."
    #Process(target=languageDetection).start()
          
        