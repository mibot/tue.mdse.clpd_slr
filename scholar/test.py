########### Python 2.7 #############
import httplib, urllib, base64

headers = {
    # Request headers
    'Content-Type': 'application/json',
    'Ocp-Apim-Subscription-Key': 'dfba48d7a4414260a3210ec13a1d3383',
}

params = urllib.urlencode({
    # Request parameters
    'numberOfLanguagesToDetect': '1',
})

try:
    conn = httplib.HTTPSConnection('https://api.cognitive.microsoft.com/sts/v1.0/issueToken')
    conn.request("POST", "/text/analytics/v2.0/languages?%s" % params, "{body}", headers)
    response = conn.getresponse()
    data = response.read()
    print(data)
    conn.close()
except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))

####################################