import os
from unicodeManager import UnicodeReader, UnicodeWriter

dataPath = os.path.abspath(os.path.relpath('../data'))

class GenerateKeywords:
    def __init__(self):
        pass
#     def keywords(self):
#         results = {'keywords': []} 
#         diff_language = {
#             "Industrial SMEs"
#         }    
    def keywords(self):
        results = {'keywords': []} 
        diff_language = {
            "Cross language",
            "Crosslanguage",
            "Cross lingual",
            "Crosslingual",
            "Cross linguistic",
            "Crosslinguistic",
            "Multi language",
            "Multilanguage",
            "Multi lingual",
            "Multilingual",
            "Multi linguistic",
            "Multilinguistic",
            "Machine translation"
        }
#         copy = {
#             "Environment"
#         }        
        copy = {
            "Copy",
            "Duplicate",
            "Plagiarism"
        }
#         detection = {
#             "Sustainable Development"
#             }
        detection = {
            "Detection",
            "Discovery"
        }
        
        for dl in diff_language:
            for cp in copy:
                for de in detection:
                    results['keywords'].append({
                    'dl': dl,
                    'cp': cp,
                    'de': de
                })
        
        return results
    
    def _save(self, result):
        for pub in result['keywords']:
            with open(os.path.join(dataPath, 'results', 'keywords_proy_inv_unemi.csv'), 'ab') as g:
                writer = UnicodeWriter(g)
                writer.writerow([pub['dl']] + [pub['cp']] + [pub['de']])
        return "Keywords saved."

# def main():
#     k = GenerateKeywords()
#     result = k.keywords()
#     k._save(result)
# 
# if __name__ == '__main__':
#     main()
            