# coding=utf-8
import requests
import os
from requests.auth import HTTPProxyAuth
from fake_useragent import UserAgent
import urllib
from bs4 import BeautifulSoup
import urllib2
import hashlib
import random
from random import randint
import time

dataPath = os.path.abspath(os.path.relpath('data/tocheck'))

proxy_host = "proxy.crawlera.com"
proxy_port = "8010"
proxy_auth = HTTPProxyAuth("0704241f69fd4731b7d77b2b6fb19acb", "")
my_proxies = {"http": "http://{}:{}/".format(proxy_host, proxy_port), "https": "https://{}:{}/".format(proxy_host, proxy_port)}

ua = UserAgent()
my_header = {'User-Agent':str(ua.random)}
        

HEADERS = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:27.0) Gecko/20100101 Firefox/27.0'}

# scihub_choices = ['https://sci-hub.ac/',
#                   'http://sci-hub.cc/',
#                   'http://sci-hub.io/',                                   
#                  'https://sci-hub.bz/',]

scihub_choices = ['https://sci-hub.tw/',
                            'http://sci-hub.tv/',
                            'http://sci-hub.la/',
                            'http://sci-hub.tv/',
                            'http://tree.sci-hub.la/',
                            ]
 

#     return {'http': 'http://%s' % proxy, 'https': 'http://%s' % proxy}

#SCIHUB_BASE_URL = 'http://sci-hub.cc/'   

class DownloadPDF():
    def __init__(self):
        pass
    
    def download(self,identifier, destination='', path = None, sh = False):   
        res = False
        #print "sh: %s" %(sh)
        data = self.fetch(identifier, sh)        
        
        if not 'err' in data:
            self._save(data['pdf'], 
                       os.path.join(destination, path if path else data['name']))
            res = True
        elif 'err' in data and sh == False:
            time.sleep(randint(10, 20))
            self.download(identifier, destination, path, sh = True)      
                
        
        return res
    
    
    def fetch(self, identifier, sh):
        if sh == False:
            url = self._get_direct_url(identifier)
        else: 
            url = self._search_direct_url(identifier)
        
        
        try:
            res = requests.get(url, headers = my_header)
            content_type = res.headers.get('content-type')
                
            if 'application/pdf' in str(content_type):
                return {
                    'pdf': res.content,
                    'url': url
                    #'name': self._generate_name(res)
                }
            else:            
                return {
                    'err': 'Failed to fetch pdf with identifier %s (resolved url %s) due to captcha' 
                       % (identifier, url)
                }
            
    
        except requests.exceptions.RequestException as e:
    
            return {
                'err': 'Failed to fetch pdf with identifier %s (resolved url %s) due to request exception.' 
                   % (identifier, url)
            }
    
    
    def _get_direct_url(self, identifier):    
        id_type = self._classify(identifier)
        
        return identifier if id_type == 'url-direct' \
                else self._search_direct_url(identifier)
    
    
    def _search_direct_url(self, identifier):
        SCIHUB_BASE_URL = random.choice(scihub_choices)
        #print SCIHUB_BASE_URL
        try:   
            res = requests.get(SCIHUB_BASE_URL + identifier, headers = my_header, proxies = my_proxies, auth=proxy_auth, verify='crawlera-ca.crt')
            
            s = self._get_soup(res.content)
            iframe = s.find('iframe')
            if iframe:
                return iframe.get('src') if not iframe.get('src').startswith('//') \
                   else 'http:' + iframe.get('src')
        
#         except TypeError:
#             return {
#                 'err': 'Failed to fetch sci-hub page with identifier during string formatting.' 
#                    % (identifier)
#             } 
        
        except requests.exceptions.RequestException as e:
    
            return {
                'err': 'Failed to fetch sci-hub page with identifier %s due to request exception.' 
                   % (identifier)
            } 
    
    
    def _classify(self, identifier):
        
        if (identifier.startswith('http') or identifier.startswith('https')):
            if 'pdf' in identifier:
                return 'url-direct'
            else:
                return 'url-non-direct'
        elif identifier.isdigit():
            return 'pmid'
        else:
            return 'doi'  
        
    def _save(self, data, path):    
        with open(path, 'wb') as f:
            f.write(data)
        #print "downloaded"
    
    def _get_soup(self, html):    
        return BeautifulSoup(html, 'html.parser')
    
    def _generate_name(self, res):
        name = res.url.split('/')[-1]
        pdf_hash = hashlib.md5(res.content).hexdigest()
        return '%s-%s' % (pdf_hash, name[-20:])
            
#result = download('http://dl.acm.org/citation.cfm?id=1045217', path='paper.pdf')