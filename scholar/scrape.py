import re
import os
import time
import urllib
import requests
from random import randint
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
from requests.auth import HTTPProxyAuth
from unicodeManager import UnicodeReader, UnicodeWriter
# import GenerateKeywords
# import random
import ssl
import urllib3
import urllib2
urllib3.disable_warnings()

dataPath = os.path.abspath(os.path.relpath('../data'))



 
    
#     proxy_choices = ["192.126.154.248:3128",
#                      "173.232.14.253:3128",
#                      "173.234.58.74:3128",
#                      "173.234.58.11:3128", 
#                      "192.126.154.197:3128", 
#                      "192.126.154.76:3128",                  
#                      "173.232.14.11:3128",
#                      "89.32.65.220:3128", 
#                      "89.32.65.138:3128"]
#     
#     proxy = random.choice(proxy_choices)
#     return {'http': 'http://%s' % proxy, 'https': 'http://%s' % proxy}


    
# HEADERS = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:27.0) Gecko/20100101 Firefox/27.0'}
SCHOLARS_BASE_URL = 'https://scholar.google.com'

class Scholar():
    def __init__(self):
        pass
    
    def _get_npages_max(self, nhits):
        page_max = int(nhits / 20)
        if (nhits % 20) != 0: page_max += 1
        return page_max
    
    def _get_npages_med(self, page_max):    
        page_med = int(page_max / 2)
        if (page_max % 2) != 0: page_med += 1
        return page_med
        

    def search(self, nquery, query, nhits, since = 1900, to = 2017):
        
        start = 0        
        page = 1
        count = 0
        page_max = 0
        
        results = {'papers': []}
    
        while True:
            try:
                params = urllib.urlencode({'start': start,'&q': "".join(query), '&hl=en&num=20&as_sdt=1,5&as_vis=1&as_ylo': since, '&as_yhi': to}, "UTF-8")            
                
                if count == 0:
                    res = self.connection(params)
                
            except requests.exceptions.RequestException as e:
                results['err'] = 'Failed to complete search with query %s (connection error)' % query
                return results
    
            s = self._get_soup(res.content)             
            
#             hits = s.find(name='div', attrs={'id': 'gs_ab_md'})
#             
#             if not hits:
#                 if 'CaptchaRedirect' in res.content:
#                     results['err'] = 'Failed to complete search with query %s (captcha)' % query
#                 return results        
#         
#             if hits is not None:
#                 hits_text = hits.findAll(text=True)
#                 if hits_text is not None and len(hits_text) > 0:
#                     try:
#                         if str(hits_text[0].split()[0]) == "About":
#                             num_results = hits_text[0].split()[1]                     
#                         elif str(hits_text[0].split()[0]) == "Page":
#                             if "about" in str(hits_text[0].split()):                        
#                                 num_results = hits_text[0].split()[4]
#                             else:
#                                 num_results = hits_text[0].split()[3]
#                         else:
#                             num_results = hits_text[0].split()[0]               
#                         num_results = num_results.replace(',', '')
#                         nhits = int(num_results)
#                     except (IndexError, ValueError):
#                         pass
#                 else:
#                     nhits = None
#             
            print "Query: %s. Number of hits: %s. Since: %s To: %s" %(nquery, nhits, since, to)
#             
#             if nhits is None and not hits:
#                 return results
#             
#             page_max = self._get_npages_max(nhits)
                        
            papers = s.find_all('div', class_="gs_r")
            
#             if not papers:
#                 if 'CaptchaRedirect' in res.content:
#                     results['err'] = 'Failed to complete search with query %s (captcha)' % query
#                 return results
            if papers:                
                for paper in papers:            
                    if not paper.find('table'):
                        title = ""
                        author = ""
                        source = ""
                        main_link = ""
                        direct_link = ""
                                                
                        link = paper.find('h3', class_='gs_rt')
                        author = paper.find('div', class_='gs_a')
                        pdf = paper.find('div', class_='gs_ggs gs_fl')
                        
                        title = (re.sub(".*\\]","",link.text)).lstrip()
                        
                        year = ""
                        if len(author.text.split("-")) == 3:                            
                            year = author.text.split("-")[1] 
                            if len(year.split(",")) == 2:
                                year = year.split(",")[1]
                                year = re.findall(r"\b\d+\b", year)
                                year = map(int, year)                        
                                year = str(year).strip("[]")                           

                        author = author.text.split("-")[0]
                        
                        if pdf:
                            url = pdf.find('a')['href']
                            direct_link = pdf.find('a')['href']
                        
                        if link.find('a'):                            
                            main_link = link.find('a')['href']
                        
                        if url: 
                            source = url.split("/")[0] + "//" + url.split("/")[2]
#                         main_link = link.find('a')['href']
#                         direct_link = pdf.find('a')['href']
                        
                        
                        
                          
                        results['papers'].append({
                            'title': title,
                            'author': author,
                            'year': year,
                            'source': source,
                            'main_link': main_link,
                            'direct_link': direct_link
                        })                      
                        
                    
                time.sleep(randint(1, 20))
                start += 20
                if count == 0:
                    page += 1
                else: count = 0
            
            else: 
                if count == 5: return results
                else: count += 1

#             if page > page_max:                    
#                 return results
#             if page != 1:
#                 if page == page_max and not papers: count += 1
#                 if count >= 5: page += 1
            
    def _get_soup(self, html):    
        #return BeautifulSoup(html, 'html.parser')
        return BeautifulSoup(html, 'lxml')
    
    def _get_nhits(self, query, since, to):
        results = [] 
                       
        params = urllib.urlencode({'q': "".join(query), '&hl=en&num=20&as_sdt=1,5&as_vis=1&as_ylo': since, '&as_yhi': to}, "UTF-8")
        
        res = self.connection(params)
         
        s = self._get_soup(res.content)
    
        nhits = s.find(name='div', attrs={'id': 'gs_ab_md'})
        if not nhits:
            if 'CaptchaRedirect' in res.content:
                results['err'] = 'Failed to complete search with query %s (captcha)' % query
            return results        
         
        if nhits is not None:
            nhits_text = nhits.findAll(text=True)
            if nhits_text is not None and len(nhits_text) > 0:
                try:
                    if str(nhits_text[0].split()[0]) == "About":
                        num_results = nhits_text[0].split()[1]                     
                    elif str(nhits_text[0].split()[0]) == "Page":
                        if "about" in str(nhits_text[0].split()):                        
                            num_results = nhits_text[0].split()[4]
                        else:
                            num_results = nhits_text[0].split()[3]
                    else:
                        num_results = nhits_text[0].split()[0]               
                    num_results = num_results.replace(',', '')
                    results = int(num_results)
                except (IndexError, ValueError):
                    pass
            else:
                results = None
         
        return results
    
    def connection(self, params):
        res_status = 0
        
        proxy_host = "proxy.crawlera.com"
        proxy_port = "8010"
        proxy_auth = HTTPProxyAuth("0704241f69fd4731b7d77b2b6fb19acb", "")
        my_proxies = {"http": "http://{}:{}/".format(proxy_host, proxy_port), "https": "https://{}:{}/".format(proxy_host, proxy_port)}
        url = SCHOLARS_BASE_URL + "/scholar?" + params        
        
        while res_status != 200:
            ua = UserAgent()
            my_header = {'User-Agent':str(ua.random)}
                          
            #res = requests.get(url, headers = my_header, proxies = my_proxies, auth=proxy_auth, verify='crawlera-ca.crt')
            res = requests.get(url, headers = my_header)
            #time.sleep(randint(1, 20)) 
            res_status = res.status_code
            print res_status
        return res
        
        
            
    def _save_CSV(self, result, n):
        if result:
            name_file = "unresolved_query_proy_unemi-" + str(n)
            for pub in result['papers']:
                with open(os.path.join(dataPath, 'results', name_file+'.csv'), 'ab') as g:
                    writer = UnicodeWriter(g)
                    writer.writerow(str(n).split() + [pub['title']] + [pub['author']] + [pub['year']] + [pub['source']] + [pub['main_link']] + [pub['direct_link']])
                    
            return "Saved"
    

  
    

if __name__ == '__main__':
    query = '"Cross language" +Copy +Detection'
    count = 1
    
    print "Query: %s. Process: Test" %(count)
    
    since = 1998
    to = 2000  
    pub = Scholar()
    nhits = pub._get_nhits(query, since, to)
    print "Number of hits: %s" %(nhits)
    if nhits is None or nhits:
        result = pub.search(count, query, nhits, since, to)   
        pub._save_CSV(result, count)
        print "Done...!"
    else:
        print "Failed"    
     

        


