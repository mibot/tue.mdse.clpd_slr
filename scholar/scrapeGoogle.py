import time
from random import randint
from scholar.generater import GenerateKeywords
from scholar.scrape import Scholar
from multiprocessing import Process

def process_1900():
    since = 1900
    to = 1990
    start = 0
    
    count = 1
    step = 0
    nrows = len(results['keywords'])
    # while step < nrows:
    #     query = '"' + results['keywords'][step].get('dl') + '"' + ' +' + results['keywords'][step].get('cp') + ' +' + results['keywords'][step].get('de') 
    #     print query
    #     step += 1
#     while to <= 2017:
    while step < nrows:    
        print "Query: %s. Process: 1900" %(count)
        query = '"' + results['keywords'][step].get('dl') + '"' + ' +' + results['keywords'][step].get('cp') + ' +' + results['keywords'][step].get('de')
        print query
        s1 = Scholar()
        nhits = s1._get_nhits(query, since, to)
        print "Number of hits: %s" %(nhits)
        if nhits is not None and nhits: 
            pub = s1.search(count, query, nhits, since, to)         
            print s1._save_CSV(pub, count)
        
        if nhits is None or nhits:        
            count += 1
            step += 1
#         count = 1
#         step = 0
#         if since == 1900:        
#             since = to + 1 #1991
#             to = 1997
#         elif since == 1991:
#             since = to + 1 #1998
#             to = 2000
#         elif since == 1998:
#             since = to + 1 #2001
#             to = 2002
#         else:
#             since = to + 1
#             to = since
        #else:
            #since += 1

def process_1991():
    since = 1991
    to = 1997
    
    count = 1
    step = 0
    nrows = len(results['keywords'])
    while step < nrows:    
        print "Query: %s. Process: 1991" %(count)
        query = '"' + results['keywords'][step].get('dl') + '"' + ' +' + results['keywords'][step].get('cp') + ' +' + results['keywords'][step].get('de')
        print query
        s2 = Scholar()
        nhits = s2._get_nhits(query, since, to)
        print "Number of hits: %s" %(nhits)
        if nhits is not None and nhits: 
            pub = s2.search(count, query, nhits, since, to)         
            print s2._save_CSV(pub, count)
        
        if nhits is None or nhits:        
            count += 1
            step += 1

def process_1998():
    since = 1998
    to = 2000
    
    count = 1
    step = 0
    nrows = len(results['keywords'])
    while step < nrows:    
        print "Query: %s. Process: 1998" %(count)
        query = '"' + results['keywords'][step].get('dl') + '"' + ' +' + results['keywords'][step].get('cp') + ' +' + results['keywords'][step].get('de')
        print query
        s3 = Scholar()
        nhits = s3._get_nhits(query, since, to)
        print "Number of hits: %s" %(nhits)
        if nhits is not None and nhits: 
            pub = s3.search(count, query, nhits, since, to)         
            print s3._save_CSV(pub, count)
        
        if nhits is None or nhits:        
            count += 1
            step += 1

def process_2001():
    since = 2001
    to = 2002
    
    count = 1
    step = 0
    nrows = len(results['keywords'])
    while step < nrows:    
        print "Query: %s. Process: 2001" %(count)
        query = '"' + results['keywords'][step].get('dl') + '"' + ' +' + results['keywords'][step].get('cp') + ' +' + results['keywords'][step].get('de')
        print query
        s4 = Scholar()
        nhits = s4._get_nhits(query, since, to)
        print "Number of hits: %s" %(nhits)
        if nhits is not None and nhits: 
            pub = s4.search(count, query, nhits, since, to)         
            print s4._save_CSV(pub, count)
        
        if nhits is None or nhits:        
            count += 1
            step += 1

def process_2003():
    since = 2003
    to = 2003
    
    count = 1
    step = 0
    nrows = len(results['keywords'])
    while step < nrows:    
        print "Query: %s. Process: 2003" %(count)
        query = '"' + results['keywords'][step].get('dl') + '"' + ' +' + results['keywords'][step].get('cp') + ' +' + results['keywords'][step].get('de')
        print query
        s5 = Scholar()
        nhits = s5._get_nhits(query, since, to)
        print "Number of hits: %s" %(nhits)
        if nhits is not None and nhits: 
            pub = s5.search(count, query, nhits, since, to)         
            print s5._save_CSV(pub, count)
        
        if nhits is None or nhits:        
            count += 1
            step += 1 

def process_2004():
    since = 2004
    to = 2004
    
    count = 1
    step = 0
    nrows = len(results['keywords'])
    while step < nrows:    
        print "Query: %s. Process: 2004" %(count)
        query = '"' + results['keywords'][step].get('dl') + '"' + ' +' + results['keywords'][step].get('cp') + ' +' + results['keywords'][step].get('de')
        print query
        s6 = Scholar()
        nhits = s6._get_nhits(query, since, to)
        print "Number of hits: %s" %(nhits)
        if nhits is not None and nhits: 
            pub = s6.search(count, query, nhits, since, to)         
            print s6._save_CSV(pub, count)
        
        if nhits is None or nhits:        
            count += 1
            step += 1 

def process_2005():
    since = 2005
    to = 2005
    
    count = 1
    step = 0
    nrows = len(results['keywords'])
    while step < nrows:    
        print "Query: %s. Process: 2005" %(count)
        query = '"' + results['keywords'][step].get('dl') + '"' + ' +' + results['keywords'][step].get('cp') + ' +' + results['keywords'][step].get('de')
        print query
        s7 = Scholar()
        nhits = s7._get_nhits(query, since, to)
        print "Number of hits: %s" %(nhits)
        if nhits is not None and nhits: 
            pub = s7.search(count, query, nhits, since, to)         
            print s7._save_CSV(pub, count)
        
        if nhits is None or nhits:        
            count += 1
            step += 1  

def process_2006():
    since = 2006
    to = 2006
    
    count = 1
    step = 0
    nrows = len(results['keywords'])
    while step < nrows:    
        print "Query: %s. Process: 2006" %(count)
        query = '"' + results['keywords'][step].get('dl') + '"' + ' +' + results['keywords'][step].get('cp') + ' +' + results['keywords'][step].get('de')
        print query
        s8 = Scholar()
        nhits = s8._get_nhits(query, since, to)
        print "Number of hits: %s" %(nhits)
        if nhits is not None and nhits: 
            pub = s8.search(count, query, nhits, since, to)         
            print s8._save_CSV(pub, count)
        
        if nhits is None or nhits:        
            count += 1
            step += 1 

def process_2007():
    since = 2007
    to = 2007
    
    count = 1
    step = 0
    nrows = len(results['keywords'])
    while step < nrows:    
        print "Query: %s. Process: 2007" %(count)
        query = '"' + results['keywords'][step].get('dl') + '"' + ' +' + results['keywords'][step].get('cp') + ' +' + results['keywords'][step].get('de')
        print query
        s9 = Scholar()
        nhits = s9._get_nhits(query, since, to)
        print "Number of hits: %s" %(nhits)
        if nhits is not None and nhits: 
            pub = s9.search(count, query, nhits, since, to)         
            print s9._save_CSV(pub, count)
        
        if nhits is None or nhits:        
            count += 1
            step += 1  

def process_2008():
    since = 2008
    to = 2008
    
    count = 1
    step = 0
    nrows = len(results['keywords'])
    while step < nrows:    
        print "Query: %s. Process: 2008" %(count)
        query = '"' + results['keywords'][step].get('dl') + '"' + ' +' + results['keywords'][step].get('cp') + ' +' + results['keywords'][step].get('de')
        print query
        s10 = Scholar()
        nhits = s10._get_nhits(query, since, to)
        print "Number of hits: %s" %(nhits)
        if nhits is not None and nhits: 
            pub = s10.search(count, query, nhits, since, to)         
            print s10._save_CSV(pub, count)
        
        if nhits is None or nhits:        
            count += 1
            step += 1 
                                               
if __name__ == '__main__':
    k = GenerateKeywords()
    results = k.keywords()
    
    Process(target=process_1900).start()
    Process(target=process_1991).start()
    Process(target=process_1998).start()
    Process(target=process_2001).start() 
    Process(target=process_2003).start()  
    Process(target=process_2004).start()  
    Process(target=process_2005).start()  
    Process(target=process_2006).start()  
    Process(target=process_2007).start()
    Process(target=process_2008).start()           
    